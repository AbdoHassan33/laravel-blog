<?php

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=> ['web','auth']], function(){
    Route::get('posts','PostController@showAllPosts')->name('post-showall');
    Route::get('post/create','PostController@create')->name('post-create');
    Route::get('post/{id}/edit','PostController@create')->name('post-create');
    Route::post('post/create','PostController@createSubmit')->name('post_createSubmit');
    Route::post('post/update','PostController@update')->name('post-update');
    Route::post('post/delete','PostController@delete')->name('post-create');

});


Route::get('/', 'PageController@index')->name('index');
Route::get('/faq', 'PageController@faq')->name('faq');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/explore', 'PageController@explore')->name('explore');


Auth::routes();

