@include('partials.header')

<body>
@include('partials.nav')

@yield('content')

@include('partials.footer')

@include('partials.scripts')


</body>