<?php

namespace App\Http\Controllers;
use App\User;
use App\Post;
use Session;
use Illuminate\Foundation\Auth;

use Illuminate\Http\Request;

class PostController extends Controller
{

    public function showAllPosts(){
        $posts = Post::where('user_id', Auth::id());

        return view('posts.ShowAll', ['posts'=>$posts]);
    }

    public function create(){
        return view('posts.create');
    }


    public function createSubmit(Request $request){
        $validatedData = $request->validate([
            'title' => 'required',
            'body' => 'required',
            'slug' => 'required|unique:posts',
        ]);
        $post = new Post();
        $post->title = $validatedData['title'];
        $post->user_id = Auth::id();
        $post->body = $validatedData['body'];
        $post->slug = str_replace(' ','-',$validatedData['slug']); 
        Session::flash('success','your post has been added');

    }

    public function edit($id){
        return view('post.create');
    }


    public function update(Request $request){
        return view('post.create');
    }

    public function delete(){
        return view('post.create');
    }



}
